#/bin/bash
rm teamcity-build-status.zip 2&>/dev/null
rm teamcity-build-status.crx 2&>/dev/null
if [[ ! -f key.pem ]]; then
  echo "Please place key.pem near this script"
  exit 128
fi

cd src
zip -9r -x@../.gitignore teamcity-build-status.zip .
mv teamcity-build-status.zip ../
cd ..
zip -9g teamcity-build-status.zip key.pem

CHROMIUM="$(which chromium)"
if [[ -f $CHROMIUM ]]; then
  $CHROMIUM --pack-extension=src --pack-extension-key=key.pem 2&>/dev/null
  mv src.crx teamcity-build-status.crx
fi
