// Saves options to localStorage.
function save_options() {
    console.log("saving");
    var box = document.getElementById("server");

    var value = box.value;
    if (!value.endsWith('/')) {
        value += '/';
    }

    localStorage["server"] = value;
    chrome.extension.getBackgroundPage().init();
    console.log("saved");
}

// Restores select box state to saved value from localStorage.
function restore_options() {
    var server = localStorage["server"];
    if (!server) {
        return;
    }

    var tbox = document.getElementById("server");
    tbox.value = server;
}

function init() {
    restore_options();
    document.getElementById("save").addEventListener('click', save_options);
}

window.addEventListener('load', init);
